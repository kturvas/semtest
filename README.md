# Semtest

Using: https://python-semantic-release.readthedocs.io/en/latest/

This project is a python implementation of a similar npm project: https://github.com/semantic-release/semantic-release

Configured using `setup.cfg`

Releases done using command `semantic-release`

`semantic-release publish` for this repo will do the following:
1. Update changelog file.
2. Update `__version__` of python package.
3. Push changes to git.
5. Run semantic-release changelog and post to gitlab releases (in the future we can upload dist and wheel to releases via semantic-release as well, have to do it ourselves if needed now).

Versioning can be automated using CI by adding a step that runs `semantic-release publish`

## How does it know what to add to changelog, which version to bump to?

semantic-release parses commits and changes changelog, version number accoringly. Three styles of commits are possible: Angular, emoji and scipy. Default is Angular, can change to the other two in the config.

### 1. Angular style

It parses commits according to Angular commit style: https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#commits

Commit message format (The header is mandatory and the scope of the header is optional.): 
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The subject contains succinct description of the change:
- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize first letter
- no dot (.) at the end

The body should include the motivation for the change and contrast this with previous behavior.

The footer should contain any information about Breaking Changes. Breaking Changes should start with the word BREAKING CHANGE: with a space or two newlines. The rest of the commit message is then used for this.

Commit must be one of the following for version to be iterated or it to be added to changelog:
- feat: A new feature
- fix: A bug fix
- docs: Documentation only changes
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- refactor: A code change that neither fixes a bug nor adds a feature
- perf: A code change that improves performance
- test: Adding missing or correcting existing tests
- chore: Changes to the build process or auxiliary tools and libraries such as documentation generation

### 2. scipy style

Commit messages using scipy tags of the form:
```
<tag>(<scope>): <subject>
<body>
```

The elements <tag>, <scope> and <body> are optional. If no tag is present, the commit will be added to the changelog section “None” and no version increment will be performed.

To communicate a breaking change add “BREAKING CHANGE” into the body at the beginning of a paragraph. Fill this paragraph with information how to migrate from the broken behavior to the new behavior. It will be added to the “Breaking” section of the changelog.

Commit must be one of the following for version to be iterated or it to be added to changelog:
- API: an (incompatible) API change
- BENCH: changes to the benchmark suite
- BLD: change related to building 
- BUG: bug fix
- DEP: deprecate something, or remove a deprecated object
- DEV: development tool or utility
- DOC: documentation
- ENH: enhancement
- MAINT: maintenance commit (refactoring, typos, etc.)
- REV: revert an earlier commit
- STY: style fix (whitespace, PEP8)
- TST: addition or modification of tests
- REL: related to releasing 


Supported Changelog Sections:

breaking, feature, fix, Other, None

### 3. emoji style

If a commit contains multiple emojis, the one with the highest priority (major, minor, patch, none) or the one listed first is used as the changelog section for that commit. Commits containing no emojis go into an “Other” section. Emojis(major, minor, patch): https://python-semantic-release.readthedocs.io/en/latest/configuration.html#config-major-emoji


#### major_emoji

Comma-separated list of emojis used by semantic_release.history.emoji_parser() to create major releases.

Default: :boom:

#### minor_emoji

Comma-separated list of emojis used by semantic_release.history.emoji_parser() to create minor releases.

Default: :sparkles:, :children_crossing:, :lipstick:, :iphone:, :egg:, :chart_with_upwards_trend:

#### patch_emoji

Comma-separated list of emojis used by semantic_release.history.emoji_parser() to create patch releases.

Default: :ambulance:, :lock:, :bug:, :zap:, :goal_net:, :alien:, :wheelchair:, :speech_balloon:, :mag:, :apple:, :penguin:, :checkered_flag:, :robot:, :green_apple:

