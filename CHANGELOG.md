# Changelog

<!--next-version-placeholder-->

## v2.3.1 (2022-03-29)
### Fix
* Deleted whitespace from config for testing ([`0b101d6`](https://gitlab.cern.ch/kturvas/semtest/-/commit/0b101d63d5ad3319012e10d6215030526496b823))

## v2.3.0 (2022-03-29)
### Fix
* CI ([`0b9db7c`](https://gitlab.cern.ch/kturvas/semtest/-/commit/0b9db7c4a2fdcc7376c0ebe27a985e52832bad91))
* CI ([`1a3cbcb`](https://gitlab.cern.ch/kturvas/semtest/-/commit/1a3cbcbf3f29cf6717ed91445e28aa56f23a4dfe))

## v2.2.0 (2022-03-29)


## v2.1.0 (2022-03-28)
### Feature
* Readme changes ([`f221f7c`](https://gitlab.cern.ch/kturvas/semtest/-/commit/f221f7ca89725ce0b8113b008d4da74dc16a3d94))

## v2.0.0 (2022-03-28)
### Fix
* Readme changes ([`056128b`](https://gitlab.cern.ch/kturvas/semtest/-/commit/056128b69d1dc928ea43376d8e7126813781702c))

### Breaking
* yes very much breakes the older readme  ([`056128b`](https://gitlab.cern.ch/kturvas/semtest/-/commit/056128b69d1dc928ea43376d8e7126813781702c))

## v1.2.1 (2022-03-28)
### Fix
* Commit ver nr ([`b455979`](https://gitlab.cern.ch/kturvas/semtest/-/commit/b4559795589863e463336d6a2e3afc1a77eff095))

## v1.0.0 (2022-03-28)


## v0.2.5 (2022-03-28)
### Fix
* Dist path ([`4459e6a`](https://gitlab.cern.ch/kturvas/semtest/-/commit/4459e6a22b77553beb67120c4783b868ff22df4a))

## v0.2.4 (2022-03-28)
### Fix
* Test ([`f312701`](https://gitlab.cern.ch/kturvas/semtest/-/commit/f31270118a1cb6d5d7056889a7d8c3018c3177e0))

## v0.2.3 (2022-03-28)
### Fix
* Remove pypi, test for hvcs ([`0e6e7c8`](https://gitlab.cern.ch/kturvas/semtest/-/commit/0e6e7c8bec04e77f697454e4438c3460e82a74a3))

## v0.2.2 (2022-03-28)
### Fix
* Dont release to pypi ([`dabe133`](https://gitlab.cern.ch/kturvas/semtest/-/commit/dabe133c65984f0de53502330738c630b2ca74ac))

## v0.2.0 (2022-03-28)
### Feature
* Version control ([`eb472a2`](https://gitlab.com/kturvas/semtest/-/commit/eb472a2c34e5030e41112ad9f9b89f522203a472))
* Changelog ([`bd9a45f`](https://gitlab.com/kturvas/semtest/-/commit/bd9a45f779582c4607a8d6a67bbcb6248fb8e0f5))

## v0.1.1 (2022-03-28)
### Fix
* Tag instead of commit ([`64a9f20`](https://github.com/kturvas/semtest/commit/64a9f200cbccac44930f5cf64003c66fd7e8f69e))
