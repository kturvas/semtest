"""
High-level tests for the  package.

"""

import semtest


def test_version():
    assert semtest.__version__ is not None
